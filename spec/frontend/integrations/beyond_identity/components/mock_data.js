export const exclusionsMock = [
  { id: 1, name: 'foo', type: 'project', icon: 'project', avatarUrl: 'foo.png' },
  { id: 2, name: 'bar', type: 'project', icon: 'project', avatarUrl: 'bar.png' },
];
